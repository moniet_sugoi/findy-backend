require("@tensorflow/tfjs-node");
const tf = require("@tensorflow/tfjs");
const faceapi = require('face-api.js')  
const canvas = require('canvas')
const path = require('path')
const { Canvas, Image, ImageData } = canvas  
const faceDetectionNet = faceapi.nets.ssdMobilenetv1

faceapi.env.monkeyPatch({ Canvas, Image, ImageData })

const MODELS_URL = path.join(__dirname, '../public/weights');


// SsdMobilenetv1Options
const minConfidence = 0.5

// TinyFaceDetectorOptions
const inputSize = 408
const scoreThreshold = 0.5

// MtcnnOptions
const minFaceSize = 50
const scaleFactor = 0.8

function getFaceDetectorOptions(net) {
  return net === faceapi.nets.ssdMobilenetv1
    ? new faceapi.SsdMobilenetv1Options({ minConfidence })
    : (net === faceapi.nets.tinyFaceDetector
      ? new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
      : new faceapi.MtcnnOptions({ minFaceSize, scaleFactor })
    )
}

const faceDetectionOptions = getFaceDetectorOptions(faceDetectionNet)

module.exports = async function findFace(imagesKeys, userImage, localImage) {
  console.log(userImage)
    console.log('====starting=====')
    console.time('loadImage')
    let image = await canvas.loadImage(localImage)
    console.timeEnd('loadImage')
    console.time('loadUserImage')
    const userCanvasImage = await canvas.loadImage(userImage)
    console.timeEnd('loadUserImage')

    // initialize faceMatcher with faces from currentImage
    console.time('detectAllFaces')
    let referenceImage = await faceapi.detectAllFaces(image).withFaceLandmarks().withFaceDescriptors()
    console.timeEnd('detectAllFaces')
    

    if (referenceImage.length === 0) { // if no faces detected, check for single face  
      let singleFaceReferenceImage = await faceapi.detectSingleFace(image).withFaceLandmarks().withFaceDescriptor()
      return null
    }
  
    const faceMatcher = new faceapi.FaceMatcher(referenceImage)


    console.time('detectUserFace')
    const userDescriptor = await faceapi.detectSingleFace(userCanvasImage).withFaceLandmarks().withFaceDescriptor()
    console.timeEnd('detectUserFace')

    if (userDescriptor) {
      console.time('findbestmatch')
      const bestMatch = faceMatcher.findBestMatch(userDescriptor.descriptor)
      console.timeEnd('findbestmatch')
        return {
            bestMatch,
            localImage
        }
    }
}

