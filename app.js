require("@tensorflow/tfjs-node");
const express = require('express')
const fileUpload = require('express-fileupload');
const app = express()
const fs = require('fs')
const cors = require('cors')
const port = 4000
const bodyParser = require('body-parser')
const promisify = require('util').promisify
const readdir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)
const tf = require("@tensorflow/tfjs");
const faceapi = require('face-api.js')  
const canvas = require('canvas')  
const path = require('path')
const findFace = require('./utils/FR')
const async = require('async')
const imagemin = require('imagemin')
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');

const MODELS_URL = path.join(__dirname, './public/weights');
const IMAGES_DIRECTORY = './public/images'

// middleware
app.use(fileUpload({
    createParentPath: true
}));
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('./public/images'))
// app.use(async (req, res, next) => {
    

//     console.log('files before ===', req.files)
//     let image = req.files['image-0']
//     let imageBuffer = image.data
//     let size = image.size

//     if (size <= 200000) next()

//     let compressedImage = await imagemin.buffer(imageBuffer, {
//         plugins: [
//             imageminMozjpeg(0),
//             imageminPngquant({
//                 quality: [0.6, 0.8]
//             })
//         ]
//     })

//     req.files['image-0'] = {
//         ...req.files['image-0'],
//         data: compressedImage
//     }

//     console.log('files after ===', req.files)
//     next()
// })


app.post('/find-images', async (req, res) => {
    let files = req.files
    if (!files) {
        return res.send({
            status: false,
            message: 'No file uploaded'
        }).status(401)
    } 

    const imageKeys = Object.keys(files)
    const userImage = files['image-0'].data
    const filenames = await readdir(IMAGES_DIRECTORY)

    await faceapi.nets.faceRecognitionNet.loadFromDisk(MODELS_URL)
    await faceapi.nets.faceLandmark68Net.loadFromDisk(MODELS_URL)
    await faceapi.nets.ssdMobilenetv1.loadFromDisk(MODELS_URL)
    await faceapi.nets.tinyFaceDetector.loadFromDisk(MODELS_URL)

    async function queryFile(file) {
        let localImage = await readFile(IMAGES_DIRECTORY + '/' + file)
        let type = file.split('.')[1].toLowerCase()
        let match = await findFace(imageKeys, userImage, localImage)

        if (match && match.bestMatch._distance < 0.6) {
            // let val = {
            //     buffer: Buffer.from(match.localImage).toString('base64'),
            //     type
            // }
            // console.log(match.bestMatch._distance)
            return file
        } 
    }

    console.time('image_processing')
        let data = await Promise.all(filenames.map(async file => queryFile(file)))
        res.json({data: data.filter(file => !!file)}).status(200)
    console.timeEnd('image_processing')
})

app.listen(port, console.log(`listening on port ${port}`))