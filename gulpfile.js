var gulp = require('gulp');
const imagemin = require('gulp-imagemin');

function imageCompress(cb) {
  return gulp.src('./public/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('/compressed-images'));
  cb();
}

function imageWatch(cb) {
  gulp.watch('./public/images/*', gulp.series(imageCompress));
  cb();
}

gulp.task('default', gulp.series(imageWatch, imageCompress));
